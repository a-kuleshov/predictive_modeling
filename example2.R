library('AppliedPredictiveModeling')
library(caret)
#x <- 1:12;
#sample(x);
#sample(x, replace = TRUE)

data(twoClassData)
str(predictors)
str(classes)

set.seed(1)

trainingRows <- createDataPartition(classes, p = 0.8, list = FALSE)
trainingRows
trainPrefictors <- predictors[trainingRows,]
trainClasses <- classes[trainingRows]

testPredictors <- predictors[-trainingRows,]
testClasses <- classes[-trainingRows]
str(trainPrefictors)

repeatedSplits <- createDataPartition(trainClasses, p = 0.8, times = 3)
str(repeatedSplits)
cvSplits <- createFolds(trainClasses, k = 10, returnTrain = TRUE)
str(cvSplits)

# fold1 <-cvSplits

trainPrefictorsMat <-as.matrix(trainPrefictors)
knnFit <- knn3(x = trainPrefictorsMat, y = trainClasses, k = 5)
knnFit
knn3.matrix(x = trainPrefictors, y = trainClasses, k= 5)


knn3.matrix(x = testPrefictors, y = testClasses, k= 5)

testPredictions <- predict(knnFit, newdata = testPredictors, type = "class")
testPredictions
testPredictors
